const startButton = document.querySelector('.start');
const loader = document.querySelector('.loader');
const numero = document.querySelector('.numero');

function imprimirNumero(n) {
  numero.textContent = n;
}

function muestraLoading() {
  loader.removeAttribute('hidden');
}

function ocultaLoading() {
  loader.setAttribute('hidden', true);
}

function muestraBoton() {
  startButton.removeAttribute('hidden');
}

function ocultaBoton() {
  startButton.setAttribute('hidden', true);
}

function random() {
  return Math.random() * 10;
}
